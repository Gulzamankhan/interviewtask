import React from 'react';

import './App.css';
import Menu from './Components/Menu';
import Home from './Components/Home';
import RegistrationForm  from './Components/RegistrationForm'
import { BrowserRouter, Route,Switch} from "react-router-dom";
import show from './Components/show';
function App() {
  return (
    
    <BrowserRouter>
    <div className="App">
    <Menu></Menu>
      <Route path='/' exact component={Home}></Route>
    <Route path='/RegistrationForm' component={RegistrationForm}/>
    <Route path='/show' component={show}/>
    <Switch></Switch>


    </div>
    </BrowserRouter>
  );
}

export default App;
