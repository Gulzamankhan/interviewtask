import React, { Component } from 'react'
import './Menu.css'
import logo from '../iiui-logo.jpg'
import'bootstrap/dist/css/bootstrap.css'
import {Link} from 'react-router-dom'
export class Menu extends Component {
    render() {
        return (
            <div>
             
                 <img src={logo} className='logo' alt="not found"></img>
                      <ul>
<li><Link to="show">Show Information</Link></li>
  <li><Link  to="RegistrationForm">New Registration</Link></li>
  <li><Link to="/">Home</Link></li>
</ul>
            </div>   
        )
    }
}
export default Menu
