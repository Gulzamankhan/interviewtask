import React from 'react'
import {connect} from 'react-redux'
import './show.css'
function show(props) {
    //console.log(props)
    
    return (
      
        <div>
            <table>
            <tbody >
  <tr>
  <th>First Name</th>
  <th>Last Name</th>
  <th>Adress</th>
  <th>Company</th>
  <th>Phone</th>
  <th>DOB</th>
  </tr>
  <tr>
    <br></br>
  <td>{props.f_Name}</td>
  <td>{props.l_name}</td>
  <td>{props.adress}</td>
  <td>{props.company}</td>
  <td>{props.Phone}</td>
  <td>{props.dob}</td>
  </tr>
</tbody>
</table>
      </div>
    )
}
 const mapStateToProps=(state)=>
{
  return {
    f_Name:state.f_Name,
    l_name:state.l_Name,
    adress:state.adress,
    company:state.Company,
     Phone:state.phone,
     dob:state.dob
  }
}

export default connect(mapStateToProps)(show)
