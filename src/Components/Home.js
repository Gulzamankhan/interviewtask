import React, { Component } from 'react'
import background from '../home.jpg'
import about from '../about_iiui.jpg'
import './Home.css'
export class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
             image_path:'../IIUI_Lawn.jpg'
        }
    }
    render() {
        return (
            <div>
                <img src={background} className='homeimg' alt="not"></img>
                <div className='aboutiiui'>
                    <img src={about} className='aboutimg' alt="NOT fOUND"></img>
                    <br></br><br></br>
                    <h1>International Islamic University Islamabad H-10 Sector Islamabad</h1>
                    <h5>
                          The foundation of the Islamic University, Islamabad was laid on the first day
                         of the fifteenth century Hijrah i.e. Muharram 1, 1401 (November 11,1980).
                         This landmark of the beginning of the new Century symbolizes the aspirations and 
                         hopes of the Muslim Ummah for an Islamic renaissance. The university was created to 
                         produce scholars and practitioners who are imbued with Islamic ideology, whose character 
                         and personality conforms to the teachings of Islam, and who are capable to cater 
                         to the economic, social, political, technological and intellectual needs of the Muslim Ummah. 
                         </h5>
                         <hr></hr>
                </div>
                <div className='events'>
                
                </div>
            </div>
        )
    }
}

export default Home
