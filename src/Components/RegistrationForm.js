import React, { Component } from 'react'
import  './Form.css'
import {connect} from 'react-redux'
 class RegistrationForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
             f_Name:"",
             l_Name:"",
             adress:"",
             Company:"",
             phone:"",
             dob:"",
             f_NameErro:"",
             l_NameError:"",
             adressError:"",
             comapnyError:"",
             phoneError:"",
             dobError:""
        }
    }
     validateLname(lname) { 
         return lname.match(/^[a-zA-Z\s]{1,50}$/);
     }
     validateFname(fname) {
        return fname.match(/^[a-zA-Z\s]{1,50}$/);
     }
     validateadress(adress) {
        return adress.match(/^[a-zA-Z\s]{1,50}$/);
     }
     validateCompany(company) {
        return company.match(/^[a-zA-Z\s]{1,50}$/);
     }
     validatePhone(phone) {
        return phone.match(/^[0]+[3]+[0-9]{9}$/);
     }
     validateDob(dob)
     {
         if(dob==="")
         {
            
             return false;
         }
         else{
           

             return true;
         }
     }
    handleSubmit=(event)=>
    {
     event.preventDefault();

     if(this.validateFname(this.state.f_Name))
     {
        this.setState({
            f_NameErro:""
        })
     }
     else{
         this.setState({
             f_NameErro:"invalid!"
         })
     }
     if(this.validateLname(this.state.l_Name))
     {   
        this.setState({
            l_NameError:""
        })

     }
     else{
         this.setState({
             l_NameError:"invalid!"
         })
     }

     if(this.validateadress(this.state.adress))
     {
        
        this.setState({
            adressError:""
        })
    
     }
     else{
         this.setState({
             adressError:"invalid!"
         })
     }
     if(this.validateCompany(this.state.Company))
     {
        
        this.setState({
            companyError:""
        })
    
     }
     else{
         this.setState({
             companyError:"invalid!"
         })
     }
     if(this.validatePhone(this.state.phone))
     {
        this.setState({
            phoneError:""
        })  
    
     }
     else{
         this.setState({
             phoneError:"invalid!"
         })
     }
    if(!this.validateDob(this.state.dob))
    {
        this.setState({
            dobError:"Required"
        })
    }
    else{
        this.setState({
            dobError:""
        })
    }
     if(this.validateFname(this.state.f_Name)&&this.validatePhone(this.state.phone)&&this.validateCompany(this.state.Company)&&this.validateadress(this.state.adress)&&this.validateLname(this.state.l_Name)&&this.validateFname(this.state.f_Name)&&this.validateDob(this.state.dob))
     {
         alert("Information Stored Succesfully!")
         
        //reducer(this.state)
     }
     
    }
    handlechange=(event)=>
    {
        this.setState({
           
        [event.target.name]:event.target.value
             })     
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <br></br><br></br>
                    <h4>Employee Registration  Form</h4>
  <label name="fname">First Name</label>
  <input type="text" id="fname" name="f_Name" value={this.state.f_Name} onChange={this.handlechange}/>
  <span className="error" id="FnameError" value="">{this.state.f_NameErro}</span>
  <label name="lname">Last Name</label>
  <input type="text" id="lname" name="l_Name" value={this.state.l_Name} onChange={this.handlechange}/>
  <span className="error" id="lnameError" value="">{this.state.l_NameError}</span>
  <br></br>
  <label name="adress">Adress</label>
  <input type="text" id="adress" name="adress" value={this.state.adress} onChange={this.handlechange}/>
  <span className="error" id="lnameError" value="">{this.state.adressError}</span>
  <label name="cname">Company</label>
  <input type="text" id="cname" name="Company" value={this.state.Company} onChange={this.handlechange}/>
  <span className="error" id="lnameError" value="">{this.state.companyError}</span>
  <br></br>
  <label name="adress">Phone(Mob) </label>
  <input type="number" id="phone" name="phone" value={this.state.phone} onChange={this.handlechange}/>
  <span className="error" id="lnameError" value="">{this.state.phoneError}</span>
  <label name="cname">DOB</label>
  <input type="date" id="dob" name="dob" value={this.state.dob} onChange={this.handlechange}/>
  <span className="error" id="dobError" value="">{this.state.dobError}</span>
  <br></br>
 <button onClick={()=>{this.props.changedata(this.state.f_Name,this.state.l_Name,this.state.adress,this.state.Company,this.state.phone,this.state.dob)}} className="button"> click</button>

</form>

            </div>
        )
    }
}
const mapStateToProps=(state)=>
{
  return {
    f_Name:state.f_Name,
    l_name:state.l_Name,
    adress:state.adress,
    company:state.Company,
     Phone:state.phone,
     dob:state.dob

    //id:state.id

  }
}

const mapdispatchprops=(dispatch)=>
{
    return{
        changedata:(fname,lname,adress,company,phone,dob)=>{dispatch({ type:'SAVE_DATA',fname,lname,adress,company,phone,dob})}
    }
}
export default connect(mapStateToProps,mapdispatchprops)(RegistrationForm)

